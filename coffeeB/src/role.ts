import { AppDataSource } from "./data-source"
import { Role } from "./entity/Role"

AppDataSource.initialize().then(async () => {
    const roleRepository = AppDataSource.getRepository(Role);
    await roleRepository.clear();
    console.log("Inserting a new role into the database...")
    var role = new Role()
    role.id = 1
    role.name = "admin"
    await roleRepository.save(role);

    role = new Role();
    role.id = 2
    role.name = "user"
    await roleRepository.save(role);

    const list = await roleRepository.find({order:{ id: "asc"}});
    console.log(list);
}).catch(error => console.log(error))
