import { AppDataSource } from "./data-source"
import { User } from "./entity/User"

AppDataSource.initialize().then(async () => {
    const userRepository = AppDataSource.getRepository(User);
    await userRepository.clear();
    console.log("Inserting a new user into the database...")
    var user = new User()
    user.id = 1
    user.email = "admin1@email.com"
    user.password = "male"  
    user.gender = "pass1234"
    await userRepository.save(user);

    user = new User();
    user.id = 2
    user.email = "user1@email.com"
    user.password = "male"
    user.gender = "pass1234"
    await userRepository.save(user);

    const list = await userRepository.find({order:{ id: "asc"}});
    console.log(list);
}).catch(error => console.log(error))
