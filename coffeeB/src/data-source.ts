import "reflect-metadata"
import { DataSource } from "typeorm"
import { User } from "./entity/User"
import { Role } from "./entity/Role"
import { Type } from "./entity/Type"

export const AppDataSource = new DataSource({
    type: "sqlite",
    database: "coffeeB.sqlite",
    synchronize: true,
    logging: false,
    entities: [User, Role, Type]
})
