import { AppDataSource } from "./data-source"
import { Type } from "./entity/Type"

AppDataSource.initialize().then(async () => {
    const typeRepository = AppDataSource.getRepository(Type);
    await typeRepository.clear();
    console.log("Inserting a new type into the database...")
    var type = new Type()
    type.id = 1
    type.name = "drink"
    await typeRepository.save(type);

    type = new Type();
    type.id = 2
    type.name = "dessert"
    await typeRepository.save(type);

    type = new Type();
    type.id = 2
    type.name = "food"
    await typeRepository.save(type);

    const list = await typeRepository.find({order:{ id: "asc"}});
    console.log(list);
}).catch(error => console.log(error))
